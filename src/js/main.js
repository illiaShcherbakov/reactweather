//import $ from 'jquery';

import React from 'react';
import ReactDOM from 'react-dom';
import WeatherWidget from 'app.js';

ReactDOM.render(<WeatherWidget />, document.getElementById("app"));