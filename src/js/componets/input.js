
import React from 'react';

export default class Input extends React.Component {
  constructor(props){
    super(props);
  };

  changeHandle(e) {
    this.props.updateInput(e.target.value);
    //this.props.getCities();
  };


  render() {
    //(e) => this.props.getCities(e)
    //onChange={this.changeHandle.bind(this)}
    return (
      <div className="input-wrapper">
        <label htmlFor="city">Please, write the city...</label>
        <input type="text" className="city-input" id="city" value={this.props.value} onKeyUp={(e) => this.props.getCities(e)} onChange={this.changeHandle.bind(this)} />
      </div>
    )
  }
};
