import React from 'react';

export default class Weather extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      units: {
        metric: "\u2103",
        imperial: "\u2109",
        default: "\u212A"
      },
      weatherImages: {
        'Clouds': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/clouds.jpg',
        'Rain': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/rain.jpg',
        'Snow': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/snow.jpg',
        'Mist': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/mist.jpg',
        'Clear': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/clear.jpg',
        'Drizzle': 'https://dl.dropboxusercontent.com/u/84421933/weatherImages/drizzle.jpg'
      },
    }
  };

  render() {
    let img = this.state.weatherImages[this.props.weatherData.weatherCond];
    let unit = this.state.units[this.props.unit];

    return (
      <div className="weather-wrapper">
        <div className="ww-background" style={{backgroundImage: 'url("'+ img + '")'}}>
          <div className="ww-wrapper">
            <div className="ww-other">
              <p className="ww-other-addition">{this.props.weatherData.weather}</p>
              <p className="ww-other-location">{this.props.weatherData.country + ', ' + this.props.weatherData.city}</p>
            </div>
            <div className="ww-temperature">
              <span className="ww-temperature-value">{this.props.weatherData.temp}</span>
              <span className="ww-temperature-unit">{unit}</span>
            </div>
          </div>
        </div>
        <img src="https://dl.dropboxusercontent.com/u/84421933/ajax-loader.gif" alt="" className="loader"/>
      </div>
    )
  }
};