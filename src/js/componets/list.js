import React from 'react';


export default class List extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cities: []
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.cities === nextProps.cities) {
      return false;
    }
    return true;
  };

  componentWillReceiveProps(nextProps) {
    //console.log(nextProps.cities);
    this.setState({
      cities: nextProps.cities
    })
  };

  componentDidMount() {
    console.log('mounted');
    const autoCompleteUl = document.querySelector('.autocomp-ul');
    //autoCompleteUl.style.width = document.getElementById('city').offsetWidth + 'px';

    document.body.addEventListener('click', (e) => {
      console.log(e.target, e.target.classList.contains('city-input') || e.target.classList.contains('autocomp-ul-item'));
      let a = e.target.classList.contains('city-input') || e.target.classList.contains('autocomp-ul-item');
      if ( !a ) {
        this.setState({
          cities: ['']
        });
        this.forceUpdate();
      }
    });
  };

  clickHandle(e) {
    console.log('clicked on item');
    this.props.updateInputValue(e.target.innerHTML);
    this.props.getData(e.target.innerHTML);
    this.setState({
      cities: []
    });

    console.log('clicked on item', this.state.cities);
  };



  render() {

    //let input = document.querySelector('#city');
    var arr = [''];

    if ( this.state.cities[0] === '%s' ) {
      arr[0] = <li key="noCity" className="autocomp-ul-item">No such city</li>;
    } else {
      arr = this.state.cities.map((el, index) => <li key={index} className="autocomp-ul-item" onClick={this.clickHandle.bind(this)}>{el}</li>);
    }

    return (
      <ul className="autocomp-ul">
        {arr}
      </ul>
    )
  }
}