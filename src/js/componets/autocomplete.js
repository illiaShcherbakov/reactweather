import React from 'react';
import $ from 'jquery';

import Input from './input';
import List from './List';


export default class Autocomplete extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cities: [],
      inputValue: ''
    }

    this.updateInputValue = this.updateInputValue.bind(this);
  };

  componentWillMount() {
    this.debounceGetCities = debounce(this.getCities, 400);

    function debounce(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
  };


  getCities() {
    //const url = "http://gd.geobytes.com/AutoCompleteCity?&template=<geobytes%20city>&q=" + input.value;
    //console.log(this.state.inputValue);

    $.getJSON(
      "http://gd.geobytes.com/AutoCompleteCity?callback=?&template=<geobytes%20city>&q=" + this.state.inputValue,
      (data) => {
        this.setState({
          cities: data
        });
      });
  };

  getDataOnKeyUp() {
    this.debounceGetCities();
  };

  updateInputValue(value){
    this.setState({
      inputValue: value
    })
  };


  render() {
    // console.log(this.props.getData);
    return (
      <div className="autocomplete-wrapper">
        <Input getCities={(e) => this.getDataOnKeyUp(e)} updateInput={this.updateInputValue} value={this.state.inputValue} />
        <List cities={this.state.cities} updateInputValue={this.updateInputValue} getData={this.props.getData} />
      </div>
    )
  }
}