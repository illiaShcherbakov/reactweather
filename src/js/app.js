import React from 'react';
import Autocomplete from './componets/autocomplete';
import Weather from './componets/weather';

class WeatherWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lang: 'en',
      unit: 'metric',
      city: 'Kharkiv',
      weatherData: {}
    };

    this.getData = this.getData.bind(this);
  };

  getData(city) {
    let url = "http://api.openweathermap.org/data/2.5/weather?q="+ city + "&units="+ this.state.unit +"&lang="+ this.state.lang +"&appid=629dbe4bc64c26fd15de637072a99882";
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);

    document.querySelector('.ww-background').classList.add('hide');
    document.querySelector('.loader').classList.add('show');

    const that = this;

    xhr.onreadystatechange = function() {
      if ( this.readyState == 4 ) {
        setTimeout(function(){
          document.querySelector('.ww-background').classList.remove('hide');
          document.querySelector('.loader').classList.remove('show');
        }, 1500);

        let parsed = JSON.parse(this.responseText);

        that.updateWeatherData(city, parsed.sys.country, parsed.main.temp, parsed.weather[0].description, parsed.weather[0].main);
      }

      if (this.status != 200) {
        // обработать ошибку
        console.log( 'ошибка: ' + (this.status ? this.statusText : 'запрос не удался') );
        //return;
      }
    };
    xhr.send();

  };

  updateWeatherData(city, country, temp, weather, weatherCond) {
    this.setState({
      weatherData: {
        city: city,
        country: country,
        weather: weather,
        weatherCond: weatherCond,
        temp: temp.toFixed(),
      },
    });
  };

  componentDidMount() {
    this.getData(this.state.city);
  };

   render() {
    return (
      <div className="ww">
        <Autocomplete getData={this.getData}/>
        <Weather weatherData={this.state.weatherData} unit={this.state.unit}/>
      </div>
    );
  }
};

export default WeatherWidget;